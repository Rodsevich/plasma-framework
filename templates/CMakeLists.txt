set(apptemplate_DIRS
    qml-plasmoid
    cpp-plasmoid)

kdetemplate_add_app_templates(${apptemplate_DIRS})
